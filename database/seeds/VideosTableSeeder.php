<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Video;
use App\NoteSet;

class VideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $video = Video::create([
            'youtube_id' => 'uU9_2BUgHcI'
        ]);

        NoteSet::create([
            'user_id' => User::first()->id,
            'video_id' => $video->id
        ]);

    }
}
