<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        // User::unguard();
        $email = $faker->email;
        User::create([
            'name' => $faker->userName,
            'email' => $email,
            'password' => $email,
        ]);
    }
}
