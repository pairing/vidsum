<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/video/add', 'VideoController@add')->name('video.add');
Route::get('/u/{user}', 'UserController@show')->name('user.show');
Route::get('/u/{user}/notesets/{noteSet}', 'NoteSetController@edit')->name('noteset.edit');
Route::get('/{id}', 'VideoController@show')->name('video.show');
