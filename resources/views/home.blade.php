@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>

            <div class="card mt-4">
                <div class="card-header">Add Video</div>
                <div class="card-body">

                <form method="POST" action="{{ route('video.add') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="youtube_url" class="col-md-4 col-form-label text-md-right">YouTube URL</label>

                            <div class="col-md-6">
                                <input id="youtube_url" type="text" class="form-control @error('youtube_url') is-invalid @enderror" name="youtube_url" value="{{ old('email') }}" required autofocus>

                                @error('youtube_url')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>

            <div class="card mt-4">
                <div class="card-header">Recent videos</div>
                <div class="card-body">


                    <ul>
                        @foreach($videos as $video)
                        <li>
                            <a href="/{{ $video->youtube_id }}">    
                                {{ $video->youtube_id }}
                            </a>
                        </li>
                        @endforeach
                        
                    </ul>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
