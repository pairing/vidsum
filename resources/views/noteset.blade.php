@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">User {{ $user->name }}</div>

                <div class="card-body">
                    <editor :video-id="'{{ $video->youtube_id }}'" />
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
