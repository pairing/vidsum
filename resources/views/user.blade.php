@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">User {{ $user->name }}</div>

                <div class="card-body">
                    :)
                </div>
            </div>

            <div class="card mt-4">
                <div class="card-header">Recent videos</div>
                <div class="card-body">


                    <ul>
                        @foreach($user->noteSets as $noteSet)
                        <li>
                            <a href="/{{ $noteSet->video->youtube_id }}">    
                                {{ $noteSet->video->youtube_id }}
                            </a>
                            <a href="{{ route('noteset.edit', [$noteSet->user, $noteSet->id]) }}">
                                Edit
                            </a>
                        </li>
                        @endforeach
                        
                    </ul>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
