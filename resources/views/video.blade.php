@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card mt-4">
                <div class="card-header">Show Video {{ $video->youtube_id }}</div>
                <div class="card-body">

                    <player :video-id="'{{ $video->youtube_id }}'" />
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
