<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    //

    public function noteSet() {
        return $this->belongsTo(NoteSet::class);
    }
}
