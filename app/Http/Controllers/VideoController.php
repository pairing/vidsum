<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Video;

class VideoController extends Controller
{
    //

    /**
     * Adds one video
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function add(Request $request)
    {
        // 1. extract youtube id from URL
        $after_v = Str::after($request->youtube_url, 'v=');
        $youtube_id = Str::before($after_v, '&');

        // 2. create video object
        $video = Video::create([
            'user_id' => Auth::id(),
            'youtube_id' => $youtube_id,
        ]);

        // 3. forward to video route
        return redirect()->route('video.show', ['id' => $video->youtube_id]);
    }

    /**
     * Shows one video
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {
        $video = Video::where("youtube_id", $id)->first();

        return view('video', [
            'video' => $video
        ]);
    }
}
