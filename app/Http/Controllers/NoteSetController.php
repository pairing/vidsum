<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NoteSet;
use App\User;

class NoteSetController extends Controller
{
    //


    /**
     * Show the user's note sets
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(User $user, NoteSet $noteSet)
    {
        return view('noteset', [
            'user' => $user,
            'noteSet' => $noteSet,
            'video' => $noteSet->video,
        ]);
    }

}
