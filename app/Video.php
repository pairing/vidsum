<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\NoteSet;

class Video extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'youtube_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'youtube_id' => 'string'
    ];

    public function NoteSets() {
        return $this->hasMany(NoteSet::class);
    }
}
