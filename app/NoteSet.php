<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Note;
use App\Video;

class NoteSet extends Model
{
    //

    public function notes() {
        return $this->hasMany(Note::class);
    }

    public function video() {
        return $this->belongsTo(Video::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
